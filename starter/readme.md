**Початковий рівень це можливість швидко спробувати інструмент без занурення.**

1. Компіляція мінімального додатку.
    
    Перше з чого треба почати, це підготуватись до можливості зробити додаток.

    Задача:
    > Встановлення та налаштування інструментів компіляції, розробки та запуску. Потім скомпілювати та запустити мінімальний додаток.

2. Взаємодія з додатком.
    Наступним кроком є можливість взаємодії користувача з додатком. Додаток як чорна коробка з якою взаємодіює користувач чи інші програми.

    Задача:
    > Зробити додаток що дозволяє ввести данні і відобразити їх

3. Обробка даних
    Додаток починати мати цінність коли у нього є функціонал.

    Задача:
    > Зробити калькулятор. Додаток повинен на вході отримати два числа і операцію обчислення: плюс, мінус, множення, ділення, а на виході повинен бути результат.

4. Логічні операції

    Задача:
    > Додаток що визначає парність чи не парне число ввів користувач

5. Цикли

    Задача:
    > Додаток що виводить заданий текст певну кількість разів. Текст та кількість задається користувачем.

6. Множина

    Задача:
    > Сортування випадкових чисел. Заповнити масив 10 випадковими числами та відсортувати від меншого до більшого.

7. Функції

    Задача:
    > Зробити функцію що буде генерувати масив випадкових натуральних чисел, і як параметри цієї функції можна задавати кількість елементів масиву, мінімальне та максимальне значення випадкового числа.

**Декілька додаткових задач:**

1. Виділити слова з тексту та зробити список 10 найпопулярніших слів.
2. Memory Matching. Побудова ігрового поля з парної кількості панелей яким відповідає парна кількість символів. Всі панелі на початкуа гри однаково. На кожному кроці гри можна відкрити тільки дві будь які панелі, якщо їм відповідають однакові символи - то вони виключаються з гри, інакше знову перегортаються.
3. Tic tac toe.
4. Wave Maze. Додаток повинен будувати лабіринт, і потім знаходити коротший шлях від стартової точки до фінішної.
5. Tetris.
6. Minesweeper.
7. Chess.
